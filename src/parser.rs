// Here is where the various combinators are imported. You can find all the combinators here:
// https://docs.rs/nom/5.0.1/nom/
// If you want to use it in your parser, you need to import it here. I've already imported a couple.

use nom::{
  IResult,
  branch::alt,
  combinator::opt,
  multi::{many1, many0},
  bytes::complete::{tag},
  character::complete::{alphanumeric1, digit1},
};

// Here are the different node types. You will use these to make your parser and your grammar.
// You may add other nodes as you see fit, but these are expected by the runtime.

#[derive(Debug, Clone)]
pub enum Node {
  Program { children: Vec<Node> },
  Statement { children: Vec<Node> },
  FunctionReturn { children: Vec<Node> },
  FunctionDefine { children: Vec<Node> },
  FunctionArguments { children: Vec<Node> },
  FunctionStatements { children: Vec<Node> },
  Expression { children: Vec<Node> },
  MathExpression {name: String, children: Vec<Node> },
  FunctionCall { name: String, children: Vec<Node> },
  VariableDefine { children: Vec<Node> },
  Number { value: i32 },
  Bool { value: bool },
  Identifier { value: String },
  String { value: String },
}

// Define production rules for an identifier
pub fn identifier(input: &str) -> IResult<&str, Node> {
  let (input, result) = alphanumeric1(input)?;              // Consume at least 1 alphanumeric character. The ? automatically unwraps the result if it's okay and bails if it is an error.
  Ok((input, Node::Identifier{ value: result.to_string()})) // Return the now partially consumed input, as well as a node with the string on it.
}

// Define an integer number
pub fn number(input: &str) -> IResult<&str, Node> {
  let (input, result) = digit1(input)?;                     // Consume at least 1 digit 0-9
  let number = result.parse::<i32>().unwrap();              // Parse the string result into a usize
  Ok((input, Node::Number{ value: number}))                 // Return the now partially consumed input with a number as well
}

pub fn boolean(input: &str) -> IResult<&str, Node> {
  println!("ABC {}", input);
  let (input, result) = alt((tag("true"), tag("false")))(input)?;
  println!("ABC RESULT {}", result);
  Ok((input, Node::Bool{value: result == "true"}))
}

pub fn string(input: &str) -> IResult<&str, Node> {
  let (input,_) = tag("\"") (input)?; 
  //let (input, result) = alphanumeric1(input)?;
  let (input, result) = many0(alt((tag(" "), alphanumeric1)))(input)?;
  let (input,_) = tag("\"") (input)?; 

  let mut strg: String = "".to_string();
  for item in result {
    strg.push_str(item);
  }
  println!("{:?}", strg);

  Ok((input, Node::String{value: strg.to_string()}))
}

pub fn function_call(input: &str) -> IResult<&str, Node> {
  println!("call {}", input);
  let (input, fn_name) = alphanumeric1(input)?;
  let (input, _) = tag("(")(input)?;
  let (input, argument) = arguments(input)?;
  let (input, _) = tag(")")(input)?;
  Ok((input, Node::FunctionCall{children: vec![argument], name: fn_name.to_string()}))
  // let mut vec = Vec::new();
  // vec.push(identify);
  // vec.extend(other_args);
}

// Math expressions with parens (1 * (2 + 3))
pub fn parenthetical_expression(input: &str) -> IResult<&str, Node> {
  let (input, op) = tag("(")(input)?;
  let (input, express) = math_expression(input)?;
  let (input, op) = tag(")")(input)?;
  Ok((input, express))
}

pub fn l4(input: &str) -> IResult<&str, Node> {
  alt((function_call, number, identifier, parenthetical_expression))(input)
}

pub fn l3_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, op) = tag("^")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l4(input)?;
  Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))
}

pub fn l3(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l4(input)?;
  let (input, tail) = many0(l3_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

pub fn l2_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, op) = alt((tag("*"),tag("/")))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l3(input)?;
  Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))
}

pub fn l2(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l3(input)?;
  let (input, tail) = many0(l2_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

// L1 - L4 handle order of operations for math expressions 
pub fn l1_infix(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, op) = alt((tag("+"),tag("-")))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, args) = l2(input)?;
  Ok((input, Node::MathExpression{name: op.to_string(), children: vec![args]}))
}

pub fn l1(input: &str) -> IResult<&str, Node> {
  let (input, mut head) = l2(input)?;
  let (input, tail) = many0(l1_infix)(input)?;
  for n in tail {
    match n {
      Node::MathExpression{name, mut children} => {
        let mut new_children = vec![head.clone()];
        new_children.append(&mut children);
        head = Node::MathExpression{name, children: new_children};
      }
      _ => () 
    };
  }
  Ok((input, head))
}

pub fn math_expression(input: &str) -> IResult<&str, Node> {
  l1(input)
}

pub fn expression(input: &str) -> IResult<&str, Node> {
  println!("expression {}", input);
  let (input, result) = alt((function_call, boolean, string, math_expression, number, identifier))(input)?;
  println!("expression out {} {:?}", input, result);
  Ok((input, Node::Expression{ children: vec![result] }))
}

pub fn statement(input: &str) -> IResult<&str, Node> {
  let (input, result) = alt((variable_define, function_return))(input)?;
  let (input, _) = tag(";")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = many0(tag("\n"))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  Ok((input, Node::Statement{ children: vec![result] }))
}

pub fn function_return(input: &str) -> IResult<&str, Node> {
  let (input, result) = tag("return ")(input)?;
  let (input, express) = expression(input)?;
  Ok((input, Node::FunctionReturn{children: vec![express]}))
}

// Define a statement of the form
// let x = expression
pub fn variable_define(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag("let ")(input)?;
  let (input, variable) = identifier(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = tag("=")(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, expression) = expression(input)?;
  Ok((input, Node::VariableDefine{ children: vec![variable, expression]}))   
}

pub fn arguments(input: &str) -> IResult<&str, Node> {
  let (input, ident) = opt(expression)(input)?;
  match ident {
    Some(id) => {
      let (input, other_args) = many0(other_arg)(input)?;
      let mut vec = Vec::new();
      vec.push(id);
      vec.extend(other_args);
      Ok((input, Node::FunctionArguments{children: vec}))
    },
    None => {
      Ok((input, Node::FunctionArguments{children: vec![]}))
    }
  }
}

// Like the first argument but with a comma in front
pub fn other_arg(input: &str) -> IResult<&str, Node> {
  let (input, _) = tag(",")(input)?;
  let (input, ident) = expression(input)?;
  Ok((input, ident))
}

pub fn function_definition(input: &str) -> IResult<&str, Node> {
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = many0(tag("\n"))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  println!("def 1 {}", input);
  let (input, _) = tag("fn ")(input)?;
  println!("def 2 {}", input);
  let (input, ident) = identifier(input)?;
  println!("def 3 {}", input);
  let (input, _) = tag("(")(input)?;
  println!("def 4 {}", input);
  let (input, args) = arguments(input)?;
  println!("def 5 {}", input);
  let (input, _) = tag(")")(input)?;
  println!("def 6 {}", input);
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = many0(tag("\n"))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  println!("def 7 {}", input);
  let (input, _) = tag("{")(input)?;  
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = many0(tag("\n"))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  println!("def 8 {}", input);
  let (input, statements) = many0(statement)(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  let (input, _) = many0(tag("\n"))(input)?;
  let (input, _) = many0(tag(" "))(input)?;
  println!("def 9 {}", input);
  let (input, _) = tag("}")(input)?;
  println!("def 10 {}", input);
  let mut vec = Vec::new();
  vec.push(ident);
  vec.push(args);
  vec.extend(statements);
  Ok((input, Node::FunctionDefine{children: vec}))
}

// Define a program. You will change this, this is just here for example.
// You'll probably want to modify this by changing it to be that a program
// is defined as at least one function definition, but maybe more. Start
// by looking up the many1() combinator and that should get you started.
pub fn program(input: &str) -> IResult<&str, Node> {
  let (input, result) = many0(alt((function_definition, statement, expression)))(input)?;  // Now that we've defined a number and an identifier, we can compose them using more combinators. Here we use the "alt" combinator to propose a choice.
  Ok((input, Node::Program{ children: result}))       // Whether the result is an identifier or a number, we attach that to the program
}
