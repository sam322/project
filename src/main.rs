extern crate nom;
extern crate cse262_project;

use cse262_project::program;

fn main() {
  let result = program(r#"fn main(){return foo(1,2,3);} fn foo(a,b,c){return a+b+c;}"#);
  println!("{:?}", result);
}
